# CODECO Demonstrator

## Overview

This project demonstrates an IoT system where a Kubernetes control cluster manages Crownstone devices. The system allows deployment of custom applications (microapps) and configuration of device workloads through a scalable and modular architecture.

## Architecture

1. **Virtual Kubelet**: Represents each Crownstone as a virtual kubelet in the Kubernetes cluster. It forwards commands to the Crownstone via a Redis message queue.
2. **Hub Scripts**: Handles BLE communication, relays commands to the Crownstones, and collects data from devices to store in a local database.
3. **Microapp**: Custom ArduinoC applications deployed to individual Crownstone devices to execute specific workloads.

### Diagram

![CODECO-kubernetes](./CODECOv1.png)

## Directory Structure

- `codeco/`: Contains deployment YAML files specifying the desired clusters
- `hub/`: Contains scripts for command handling, BLE communication, and data collection.
- `microapp/`: Contains ArduinoC code for Crownstone microapps.
- `virtual-kublet/`: Contains the implementation of the virtual kubelet for Kubernetes integration.

## Features

- **Scalable IoT Management**: Uses Kubernetes to manage Crownstone devices.
- **Custom Workloads**: Deploy custom microapps via BLE.
- **Data Aggregation**: Collect and store data from devices in a local database.
- **Dynamic Configuration**: Adjust workloads and device parameters through Kubernetes.

## Getting Started

### Prerequisites

TODO

### Installation

TODO
