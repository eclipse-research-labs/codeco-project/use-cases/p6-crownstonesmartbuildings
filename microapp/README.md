# Microapp

## Overview

This directory contains the ArduinoC microapp code that runs on Crownstone devices. The microapp handles specific workloads defined by the user and is deployed via BLE.
