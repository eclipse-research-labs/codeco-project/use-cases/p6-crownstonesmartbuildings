// Copyright (c) 2024 Almende B.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// SPDX-License-Identifier: Apache-2.0

#include <Arduino.h>
#include <ArduinoBLE.h>
#include <Mesh.h>

/**
 * A microapp for the ATC sensor which can measure Temperature
 * Sends the data via the mesh
 */

const char* peripheralAddress = "A4:C1:38:9A:45:E3";
const char* peripheralName    = "ATC_9A45E3";

// const char* peripheralAddress = "A4:C1:38:6A:69:57";
// const char* peripheralName    = "ATC_6A6957";

#define APPLICATION_ID 0xC1

const uint8_t snifferStoneId = 5;  // hub has crownstone ID ?

// callback for received peripheral advertisement
void onScannedDevice(BleDevice& device) {
	Serial.print("Microapp scan callback for ");
	Serial.println(device.address().c_str());
}

void onConnect(BleDevice& device) {
	Serial.print("Microapp connect callback for ");
	Serial.println(device.address().c_str());
}

void onNotification(BleDevice& device, BleCharacteristic& characteristic, uint8_t* data, uint16_t size) {
	Serial.print("Microapp notification for ");
	Serial.println(characteristic.uuid());
}

void setup() {
	Serial.println("BLE central ATC sensor");

	if (!BLE.begin()) {
		Serial.println("BLE.begin failed");
		return;
	}

	// Register scan handler
	if (!BLE.setEventHandler(BLEDeviceScanned, onScannedDevice)) {
		Serial.println("Setting event handler failed");
	}
	if (!BLE.setEventHandler(BLEConnected, onConnect)) {
		Serial.println("Setting event handler failed");
	}
	BLE.scanForAddress(peripheralAddress);
	Serial.println("End of setup");
}

void send2mesh(uint8_t* buffer) {
	const size_t mesgSize = 3;  // application ID (1), temperature (2)

	// Create message buf
	uint8_t messageBuf[mesgSize];

	// Copy the measurement into the mesh message
	messageBuf[0] = APPLICATION_ID;
	memcpy(&messageBuf[1], &buffer, sizeof(buffer));

	// send via mesh
	Mesh.sendMeshMsg(messageBuf, mesgSize, snifferStoneId);
}

void loop() {
	// Poll for scanned devices
	BleDevice& peripheral = BLE.available();

	if (!peripheral) {
		return;
	}

	Serial.println("Peripheral available:");
	Serial.println(peripheral.address());
	// Try to connect
	if (!peripheral.connect(10000)) {
		Serial.println("Connecting failed");
		return;
	}
	// We are looking for service with uuid 181A 'Environmental Sensing'
	if (!peripheral.discoverService("181A")) {
		Serial.println("Service discovery failed");
		peripheral.disconnect();
		return;
	}
	// Print all found characteristics
	for (uint8_t i = 0; i < peripheral.characteristicCount(); i++) {
		Serial.println(peripheral.characteristic(i).uuid());
	}

	// Check for 2A1F 'Temperature Celsius' characteristic
	if (!peripheral.hasCharacteristic("2A1F")) {
		Serial.println("No temperature char found");
		peripheral.disconnect();
		return;
	}

	// Subscribe to Temperature Characteristic
	BleCharacteristic& temperatureCharacteristic = peripheral.characteristic("2A1F");
	temperatureCharacteristic.setEventHandler(BLENotification, onNotification);
	// Subscribe to the characteristic so that we get notifications
	if (!temperatureCharacteristic.subscribe()) {
		Serial.println("Subscribing to temperature char failed");
		peripheral.disconnect();
		return;
	}

	Serial.println("Successfully subscribed to temperature characteristics");

	// intialize the buffers
	uint8_t buffer[2];

	// Notification handling loop
	while (peripheral.connected()) {
		// Poll locally for notifications
		if (temperatureCharacteristic.valueUpdated()) {
			// Read characteristic value into buffer
			if (!temperatureCharacteristic.readValue(buffer, 2)) {
				Serial.println("Reading temperature failed");
				peripheral.disconnect();
				return;
			}
			Serial.print("Temperature: ");
			Serial.println(buffer, 2);

			// Send the data via the mesh
			send2mesh(buffer);
		}

		// Without a delay, microapp may retain control too long and bluenet watchdog may trigger
		delay(1000 * 5);  // 5 sec
	}

	// Start scanning again
	BLE.scanForAddress(peripheralAddress);
}