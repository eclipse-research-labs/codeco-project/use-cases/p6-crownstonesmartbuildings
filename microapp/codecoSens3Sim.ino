// Copyright (c) 2024 Almende B.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// SPDX-License-Identifier: Apache-2.0

#include <Arduino.h>
#include <ArduinoBLE.h>
#include <Mesh.h>
#include <stdlib.h>

/**
 * A microapp acting as a BLE peripheral
 * This app simulates being a sensor which can simulate triggers for CODECO
 */

///////////////////////////////////////////////////////////////////////////////////
// Select the trigger(s) or data source(s) to report                             //
// Options: TEMP (Temperature), PACK (Packet Loss), UTIL (Scheduler Utilization) //
///////////////////////////////////////////////////////////////////////////////////
// #define TEMP 1  // Uncomment this line to report Temperature
// #define PACK 2  // Uncomment this line to report Packet Loss
#define UTIL 3  // Uncomment this line to report Scheduler Utilization
///////////////////////////////////////////////////////////////////////////////////

#define APPLICATION_ID 0xC3

const uint8_t snifferStoneId = 5;  // hub has crownstone ID ?

uint16_t loopCounter = 0;

const size_t mesgSize = 3;  // application ID (1), sensor number (1), data (1)

// Create message buf
static uint8_t messageBuf[mesgSize];

//------------------------------------------------------------------//
// Custom RNG Implementation
//------------------------------------------------------------------//
int customRandom(int min, int max) {
	return min + (rand() % (max - min));
}

//------------------------------------------------------------------//
// define a trigger or data source to report via the mesh
//------------------------------------------------------------------//
#ifdef TEMP
void updateTemperature(uint8_t* messageBuffer, bool isNormalValue) {
	if (isNormalValue) {
		// Store the temperature value in the message buffer (index 2)
		messageBuffer[2] = customRandom(20, 71);
	}
	else {
		// Store the temperature value in the message buffer (index 2)
		messageBuffer[2] = customRandom(70, 91);
	}
}
#endif
//------------------------------------------------------------------//
#ifdef PACK
void updatePacketLoss(uint8_t* messageBuffer, bool isNormalValue) {
	if (isNormalValue) {
		// Store the packet loss value in the message buffer (index 2)
		messageBuffer[2] = customRandom(0, 81);
	}
	else {
		// Store the packet loss value in the message buffer (index 2)
		messageBuffer[2] = customRandom(80, 101);
	}
}
#endif
//------------------------------------------------------------------//
#ifdef UTIL
void updateUtilization(uint8_t* messageBuffer, bool isNormalValue) {
	if (isNormalValue) {
		// Store the CPU utilization value in the message buffer (index 2)
		messageBuffer[2] = customRandom(0, 81);
	}
	else {
		// Store the CPU utilization value in the message buffer (index 2)
		messageBuffer[2] = customRandom(80, 101);
	}
}
#endif

//------------------------------------------------------------------//
// The Arduino setup function.
//------------------------------------------------------------------//

void setup() {
	Serial.begin();

	// Write something to the log (will be shown in the bluenet code as print statement).
	Serial.println("BLE peripheral simulator");

	// Copy the measurement into the mesh message
	messageBuf[0] = APPLICATION_ID;

#ifdef TEMP
	messageBuf[1] = TEMP;
#endif
#ifdef PACK
	messageBuf[1] = PACK;
#endif
#ifdef UTIL
	messageBuf[1] = UTIL;
#endif
}

//------------------------------------------------------------------//
// The Arduino loop function.
//------------------------------------------------------------------//

void loop() {

	if (loopCounter++ % 10 == 0) {
		// Calculate isNormalValue when it is time to update the value
		bool isNormalValue = customRandom(0, 100) < 95;

#ifdef TEMP
		updateTemperature(messageBuf, isNormalValue);
#endif
#ifdef PACK
		updatePacketLoss(messageBuf, isNormalValue);
#endif
#ifdef UTIL
		updateUtilization(messageBuf, isNormalValue);
#endif
	}

	// Print message buffer to serial for debugging
	Serial.println(messageBuf, 3);

	// send via mesh
	// Mesh.sendMeshMsg(messageBuf, mesgSize, snifferStoneId);

	// wait for 2 seconds
	delay(1000 * 2);
}
