//
// This file is part of the Crownstone distribution.
// Copyright (c) Crownstone (https://crownstone.rocks)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package sphere

import (
	"fmt"
	"net"
	"os"
	"strings"
)

func GuessInternalIp() string {
	var internalIP = ""

	fmt.Printf("list ips: ")

	// show available IPs
	addrs, _ := net.InterfaceAddrs()
	for _, address := range addrs {

		// Skip loopbacks
		if strings.HasPrefix(address.String(), "127.0.0.1/") || strings.HasPrefix(address.String(), "::1/") {
			continue
		}

		fmt.Printf("'%s' ", address.String())

		internalIP = address.String()
		slash := strings.Index(internalIP, "/")
		if slash > 0 {
			internalIP = internalIP[0:slash]
		}

		if internalIP != "" {
			break
		}
	}

	fmt.Print("\n")
	return internalIP
}

func PrintRelevantEnv() {
	// spill the secrets
	var envcount int = 0
	for _, e := range os.Environ() {
		kvpair := strings.SplitN(e, "=", 2)
		if strings.HasPrefix(kvpair[0], "KUBE") ||
			strings.HasPrefix(kvpair[0], "APISERVER_") ||
			strings.HasPrefix(kvpair[0], "POD_IP") {
			fmt.Printf(" - %s\t= %s\n", kvpair[0], kvpair[1])
			envcount++
		}
	}

	fmt.Printf("tymon: ENV found %d \n", envcount)
}
