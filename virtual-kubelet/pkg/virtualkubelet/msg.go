//
// This file is part of the Crownstone distribution.
// Copyright (c) Crownstone (https://crownstone.rocks)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package sphere

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
)

const QueueRequests = "vk:requests"
const QueueReplies = "vk:replies"

func getReplyQueue(node string) string {
	return QueueReplies + ":" + node
}

type RedisBridge struct {
	client *redis.Client

	requestLog sync.Map // assume type to be [string]HubRequestMessage
}

// message to be understood by the redis consumers
type HubRequestMessage struct { //nolint:golint
	Id   string            `json:"id,omitempty"`
	At   int64             `json:"at,omitempty"`
	Node string            `json:"node,omitempty"`
	Cmd  string            `json:"cmd,omitempty"`
	Args map[string]string `json:"args,omitempty"`
}

type HubReplyMessge struct { //nolint:golint
	Id     string `json:"id,omitempty"`
	At     int64  `json:"at,omitempty"`
	Result string `json:"result,omitempty"`
}

func NewRedisBridge(redisAddress string) *RedisBridge {

	fmt.Printf("redis| create client for service @ %s\n", redisAddress)

	client := redis.NewClient(&redis.Options{
		Addr:     redisAddress,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	bridge := RedisBridge{
		client: client,
	}

	bridge.requestLog = sync.Map{}

	return &bridge
}

func (r *RedisBridge) IsAlive(ctx context.Context) bool {
	// verify if it can actually connect
	var pong *redis.StatusCmd = r.client.Ping(ctx)
	if pong.String() != "ping: PONG" {
		log.Fatal("redis |", pong)
		return false
	}

	return true
}

func (r *RedisBridge) TouchNode(ctx context.Context, nodeName string) {
	//write a timestamp just for testing
	var key string = fmt.Sprintf("vk:node:%s:start", nodeName)
	var val int64 = time.Now().Unix()

	err := r.client.Set(ctx, key, val, 0).Err()
	if err != nil {
		panic(err)
	}

	oval, err := r.client.Get(ctx, key).Result()
	if err != nil {
		panic(err)
	}
	fmt.Println("redis| readback test done.", oval)
}

func (r *RedisBridge) CmdUploadMicroapp(
	ctx context.Context,
	nodename string,
	args map[string]string,
	callback func(HubReplyMessge),
) error {

	msg := HubRequestMessage{
		Id:   uuid.New().String(),
		At:   time.Now().Unix(),
		Node: nodename,
		Cmd:  "uploadMicroapp",
		Args: args,
	}

	r.requestLog.Store(msg.Id, msg)

	message_json, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	fmt.Printf("redis| push msg: %s \n", message_json)

	_ = r.client.LPush(ctx, QueueRequests, message_json)
	r.AddCallback(ctx, getReplyQueue(nodename), msg.Id, callback)

	return nil
}

func (r *RedisBridge) CmdRemoveMicroapp(
	ctx context.Context,
	nodename string,
	callback func(HubReplyMessge),
) error {

	msg := HubRequestMessage{
		Id:   uuid.New().String(),
		At:   time.Now().Unix(),
		Node: nodename,
		Cmd:  "removeMicroapp",
		Args: map[string]string{},
	}

	message_json, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	fmt.Printf("redis| push msg: %s \n", message_json)

	_ = r.client.LPush(ctx, QueueRequests, message_json)
	r.AddCallback(ctx, getReplyQueue(nodename), msg.Id, callback)

	return nil
}

// AddCallback listens to a Redis queue and triggers a callback when a specific id is received.
func (r *RedisBridge) AddCallback(
	ctx context.Context,
	queue string,
	id string,
	callback func(HubReplyMessge)) {
	go func() {
		for {
			// Check if the context is canceled
			if ctx.Err() != nil {
				return
			}

			// Listen for messages in the queue
			result, err := r.client.BLPop(ctx, 0, queue).Result()
			if err != nil {
				if ctx.Err() != nil {
					// Context-related error (cancellation)
					return
				}
				// Log non-context-related errors and exit
				fmt.Printf("redis| Error listening to Redis queue: %v\n", err)
				return
			}

			// Parse the message
			var reply HubReplyMessge
			if err := json.Unmarshal([]byte(result[1]), &reply); err != nil {
				// Log error and reinsert the invalid message to the queue
				fmt.Printf("redis| Invalid message format, reinserting: %v\n", err)
				_ = r.client.RPush(ctx, queue, result[1]).Err()
				continue
			}

			//tymon debugging: inspect logged requests
			value, loaded := r.requestLog.LoadAndDelete(reply.Id)
			if loaded {
				var req HubRequestMessage = value.(HubRequestMessage)
				fmt.Printf("matched request/response. %v -> %v\n", req, reply)
			}
			r.requestLog.Range(func(key, value interface{}) bool {
				fmt.Printf("debug: have a pending request. %v\n", value)
				return true
			})

			// Check if the ID matches
			if reply.Id == id {
				fmt.Printf("redis| Received reply: %s\n", result[1])
				callback(reply)
				return
			}

			// Reinsert the message into the queue if the ID does not match
			_ = r.client.RPush(ctx, queue, result[1]).Err()
		}
	}()
}
