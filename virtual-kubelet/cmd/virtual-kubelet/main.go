// Copyright © 2021 FORTH-ICS
// Copyright © 2017 The virtual-kubelet authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (

	// "k8s.io/client-go/rest"

	"context"
	"crypto/tls"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"

	"github.com/containerd/log"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/virtual-kubelet/virtual-kubelet/errdefs"
	"github.com/virtual-kubelet/virtual-kubelet/node"
	"github.com/virtual-kubelet/virtual-kubelet/node/api"
	"github.com/virtual-kubelet/virtual-kubelet/node/nodeutil"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apiserver/pkg/server/dynamiccertificates"

	// certificates "k8s.io/api/certificates/v1"

	// "net/http"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/client-go/informers"

	crownstoneKubelet "virtual-crownstone-kubelet/pkg/virtualkubelet"
)

func PodInformerFilter(node string) informers.SharedInformerOption {
	return informers.WithTweakListOptions(func(options *metav1.ListOptions) {
		options.FieldSelector = fields.OneTermEqualSelector("spec.nodeName", node).String()
	})
}

var (
	buildVersion = "N/A"
	buildTime    = "N/A"
	k8sVersion   = "v1.15.2" // This should follow the version of k8s.io/kubernetes we are importing
)

func main() {
	crownstoneKubelet.PrintRelevantEnv()

	ctx, cancel := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sig
		cancel()
	}()

	var opts Opts
	err := SetDefaultOpts(&opts)
	if err != nil {
		fmt.Printf("Could not set default opts...")
	}

	opts.Version = strings.Join([]string{k8sVersion, "vk", buildVersion}, "-")

	name := ""
	cmd := &cobra.Command{
		Use:   name,
		Short: name + " provides a virtual kubelet interface for your kubernetes cluster.",
		Long: name + ` implements the Kubelet interface with a pluggable
backend implementation allowing users to create kubernetes nodes without running the kubelet.
This allows users to schedule kubernetes workloads on nodes that aren't running Kubernetes.`,
		RunE: func(cmd *cobra.Command, args []string) error {
			return runVirtualKubelet(ctx, opts)
		},
	}

	installFlags(cmd.Flags(), &opts)
	if err := cmd.Execute(); err != nil && errors.Cause(err) != context.Canceled {
		log.G(ctx).Fatal(err)
	}

}

func runVirtualKubelet(ctx context.Context, c Opts) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	var taint *corev1.Taint
	if !c.DisableTaint {
		var err error
		taint, err = getTaint(c)
		if err != nil {
			return err
		}
	}

	// Ensure API client.
	clientSet, err := nodeutil.ClientsetFromEnv(c.KubeConfigPath)
	if err != nil {
		return err
	}

	// Set-up the node provider.
	mux := http.NewServeMux()
	newProvider := func(cfg nodeutil.ProviderConfig) (nodeutil.Provider, node.NodeProvider, error) {
		// rm, err := manager.NewResourceManager(cfg.Pods, cfg.Secrets, cfg.ConfigMaps, cfg.Services)
		// if err != nil {
		// 	return nil, nil, errors.Wrap(err, "could not create resource manager")
		// }

		var internalIP string = os.Getenv("POD_IP")
		if internalIP == "" {
			internalIP = crownstoneKubelet.GuessInternalIp()
			fmt.Printf("internal IP not set. use \033[35m %s \033[0m fttb\n", internalIP)
		} else {
			fmt.Printf("Internal IP set. use  \033[35m %s \033[0m fttb\n", internalIP)
		}

		// Pass our new crownstone provider to the virutal kubelet manager
		p, err := crownstoneKubelet.NewSphereProvider(
			c.ProviderConfigPath,
			c.NodeName,
			c.OperatingSystem,
			internalIP,
			c.ListenPort, c.RedisAddr)

		if err != nil {
			return nil, nil, errors.Wrapf(err, "error initializing provider %s", c.Provider)
		}

		//inject crownstone specific labels to allow others to targeting this kubelet
		if p.Xconfig.Labels != nil {
			for k, v := range p.Xconfig.Labels {
				fmt.Printf("add custom label %v=%v \n", k, v)
				cfg.Node.ObjectMeta.Labels[k] = v
			}
		}

		p.ConfigureNode(ctx, cfg.Node)
		cfg.Node.Status.NodeInfo.KubeletVersion = c.Version
		return p, nil, nil
	}

	apiConfig, err := getAPIConfig(c)
	if err != nil {
		return err
	}

	cm, err := nodeutil.NewNode(c.NodeName, newProvider, func(cfg *nodeutil.NodeConfig) error {
		cfg.KubeconfigPath = c.KubeConfigPath
		cfg.Handler = mux
		cfg.InformerResyncPeriod = c.InformerResyncPeriod

		if taint != nil {
			cfg.NodeSpec.Spec.Taints = append(cfg.NodeSpec.Spec.Taints, *taint)
		}
		cfg.NodeSpec.Status.NodeInfo.Architecture = runtime.GOARCH
		cfg.NodeSpec.Status.NodeInfo.OperatingSystem = c.OperatingSystem

		cfg.HTTPListenAddr = apiConfig.Addr
		cfg.StreamCreationTimeout = apiConfig.StreamCreationTimeout
		cfg.StreamIdleTimeout = apiConfig.StreamIdleTimeout
		cfg.DebugHTTP = true

		cfg.NumWorkers = c.PodSyncWorkers
		/*
			//inject crownstone specific taints to disallow unknown pods
			var cs_arch corev1.Taint = corev1.Taint{ Key:"kubernetes.io/arch", Value:"Armv7", Effect:corev1.TaintEffectNoSchedule, };
			var cs_os   corev1.Taint = corev1.Taint{ Key:"kubernetes.io/os", Value:"Bluenet", Effect:corev1.TaintEffectNoSchedule, };
			cfg.NodeSpec.Spec.Taints = append( cfg.NodeSpec.Spec.Taints , cs_arch, cs_os );

			//inject crownstone specific labels to allow others to targeting this kubelet
			cfg.NodeSpec.ObjectMeta.Labels[ "kubernetes.io/arch" ] = "Armv7";
			cfg.NodeSpec.ObjectMeta.Labels[ "kubernetes.io/os" ] = "Bluenet";
		*/

		return nil
	},
		nodeutil.WithClient(clientSet),
		setAuth(c.NodeName, apiConfig),
		nodeutil.WithTLSConfig(
			nodeutil.WithKeyPairFromPath(apiConfig.CertPath, apiConfig.KeyPath),
			maybeCA(apiConfig.CACertPath),
		),
		nodeutil.AttachProviderRoutes(mux),
	)
	if err != nil {
		return err
	}

	fmt.Printf("setupTracing(ctx, c) not yet supported \n")
	// if err := setupTracing(ctx, c); err != nil {
	// 	return err
	// }

	ctx = log.WithLogger(ctx, log.G(ctx).WithFields(log.Fields{
		"provider":         c.Provider,
		"operatingSystem":  c.OperatingSystem,
		"node":             c.NodeName,
		"watchedNamespace": c.KubeNamespace,
	}))

	go cm.Run(ctx) //nolint:errcheck

	defer func() {
		log.G(ctx).Debug("Waiting for controllers to be done")
		cancel()
		<-cm.Done()
	}()

	log.G(ctx).Info("Waiting for controller to be ready")
	if err := cm.WaitReady(ctx, c.StartupTimeout); err != nil {
		return err
	}

	log.G(ctx).Info("Ready")

	select {
	case <-ctx.Done():
	case <-cm.Done():
		return cm.Err()
	}
	return nil
}

func setAuth(node string, apiCfg *apiServerConfig) nodeutil.NodeOpt {
	if apiCfg.CACertPath == "" {
		return func(cfg *nodeutil.NodeConfig) error {
			cfg.Handler = api.InstrumentHandler(nodeutil.WithAuth(nodeutil.NoAuth(), cfg.Handler))
			return nil
		}
	}

	return func(cfg *nodeutil.NodeConfig) error {
		auth, err := nodeutil.WebhookAuth(cfg.Client, node, func(cfg *nodeutil.WebhookAuthConfig) error {
			var err error
			cfg.AuthnConfig.ClientCertificateCAContentProvider, err = dynamiccertificates.NewDynamicCAContentFromFile("ca-cert-bundle", apiCfg.CACertPath)
			return err
		})
		if err != nil {
			return err
		}
		cfg.Handler = api.InstrumentHandler(nodeutil.WithAuth(auth, cfg.Handler))
		return nil
	}
}

func maybeCA(p string) func(*tls.Config) error {
	if p == "" {
		return func(*tls.Config) error { return nil }
	}
	return nodeutil.WithCAFromPath(p)
}

type apiServerConfig struct {
	CertPath              string
	KeyPath               string
	CACertPath            string
	Addr                  string
	MetricsAddr           string
	StreamIdleTimeout     time.Duration
	StreamCreationTimeout time.Duration
}

func getAPIConfig(c Opts) (*apiServerConfig, error) {
	config := apiServerConfig{
		CertPath:   os.Getenv("APISERVER_CERT_LOCATION"),
		KeyPath:    os.Getenv("APISERVER_KEY_LOCATION"),
		CACertPath: os.Getenv("APISERVER_CA_CERT_LOCATION"),
	}

	config.Addr = fmt.Sprintf(":%d", c.ListenPort)
	config.MetricsAddr = c.MetricsAddr
	config.StreamIdleTimeout = c.StreamIdleTimeout
	config.StreamCreationTimeout = c.StreamCreationTimeout

	return &config, nil
}

// getTaint creates a taint using the provided key/value.
// Taint effect is read from the environment
// The taint key/value may be overwritten by the environment.
func getTaint(c Opts) (*corev1.Taint, error) {
	value := c.Provider

	key := c.TaintKey
	if key == "" {
		key = DefaultTaintKey
	}

	if c.TaintEffect == "" {
		c.TaintEffect = DefaultTaintEffect
	}

	key = getEnv("VKUBELET_TAINT_KEY", key)
	value = getEnv("VKUBELET_TAINT_VALUE", value)
	effectEnv := getEnv("VKUBELET_TAINT_EFFECT", c.TaintEffect)

	var effect corev1.TaintEffect
	switch effectEnv {
	case "NoSchedule":
		effect = corev1.TaintEffectNoSchedule
	case "NoExecute":
		effect = corev1.TaintEffectNoExecute
	case "PreferNoSchedule":
		effect = corev1.TaintEffectPreferNoSchedule
	default:
		return nil, errdefs.InvalidInputf("taint effect %q is not supported", effectEnv)
	}

	return &corev1.Taint{
		Key:    key,
		Value:  value,
		Effect: effect,
	}, nil
}
