minikube clean
minikube start

# Only needed if the docker image is not in the almende registery
eval $(minikube docker-env)
docker build -t virtual-crownstone-kubelet:1 .

# Apply minikube secrets to cluster for VK to use
kubectl create secret generic apiserver-certs \
  --from-file=vkubelet-crt.pem=$APISERVER_CERT_LOCATION \
  --from-file=vkubelet-key.pem=$APISERVER_KEY_LOCATION \
  --dry-run=client -o yaml > cfg/secret.yaml
kubectl apply -f cfg/secret.yaml

# Create a service account (why?)
kubectl apply -f cfg/service-account.yaml

# Deploy redis
kubectl apply -f cfg/redis.yaml
minikube service redis --url &

# Deploy 2 virtual kubelets
kubectl apply -f cfg/aa-aa-aa-aa-aa-aa.yaml
kubectl apply -f cfg/ff-ff-ff-ff-ff-ff.yaml

# Deploy 1 microapp
kubectl apply -f cfg/microapp.yaml
