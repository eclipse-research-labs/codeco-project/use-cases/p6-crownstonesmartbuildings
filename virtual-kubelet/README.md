# Virtual Kubelet

## Overview

This directory contains the implementation of the virtual kubelet, which represents each Crownstone device in the Kubernetes cluster. The virtual kubelet relays commands from the control cluster to the Redis message queue for execution via BLE.

## Node deployment

To deploy a virtual kubelet that represents a crownstone with mac address e0:c4:34:6d:e6:2c to a cluster, simply type:

helm install virtual-kubelet-e0-c4-34-6d-e6-2c ./chart/ --set nodeName=e0-c4-34-6d-e6-2c
