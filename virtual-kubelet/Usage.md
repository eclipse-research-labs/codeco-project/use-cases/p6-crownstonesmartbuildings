# Usage

This document provides an introduction to using the virtual-kubelet with the sphere provider. By employing this provider, a Crownstone device identified by a specific MAC address is registered as a worker node within the cluster. Each such worker node effectively represents a Crownstone in the Kubernetes environment.

Currently, the Crownstone virtual-kubelet only supports pods running micro-apps, which are application binaries designed to run alongside the Crownstone’s firmware.

The virtual-kubelet can be started as a binary, or be deployed as a pod in the cluster.

## Requirements

For deployment as pod or binary:

- docker daemon

  ```bash
  sudo curl -Lo ./get_docker.sh  https://get.docker.com
  sudo chmod +x get_docker.sh
  sudo ./get_docker.sh

  #give your user docker-group-permission
  /usr/sbin/usermod -aG docker $(whoami) ;
  ```

- a Kubernetes cluster

  One of
  - Minikube

    ```bash
    sudo curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    sudo install minikube-linux-amd64 /usr/local/bin/minikube

    minikube start

    alias kubectl="minikube kubectl --"
    ```

  - kind

    ```bash
    sudo curl -Lo ./kind  https://kind.sigs.k8s.io/dl/v0.24.0/kind-linux-amd64
    sudo chmod +x kind
    sudo mv kind /usr/local/bin/kind
    kind create cluster
    ```

- Kubernetes certificates ("ca.crt" and a "ca.key")

  - Minikube

    ```bash
    kubectl config view --minify --raw --output 'jsonpath={..user.client-certificate}' ; echo ""
    kubectl config view --minify --raw --output 'jsonpath={..user.client-key}' ; echo ""
    ```

  - kind or k3s(todo):

      ```bash
      #create 2 files
      kubectl config view --minify --raw --output 'jsonpath={..user.client-certificate-data}' | base64 -d | openssl x509 -text -out SOME_CA.CRT
      kubectl config view --minify --raw --output 'jsonpath={..user.client-key-data}' | base64 -d > SOME_CA.KEY
      ```

- An inited environment

  - minikube or kind:

      ```bash
      export KUBERNETES_SERVICE_HOST=localhost
      export KUBERNETES_SERVICE_PORT=8080
      export KUBECONFIG=/ {home_path_??} /.kube/config

      export APISERVER_CERT_LOCATION=/ {see_previous_step} /ca.crt
      export APISERVER_KEY_LOCATION=/ {see_previous_step} /ca.key
      ```

  - k3s(todo):

      ```bash
      export KUBECONFIG=/path_to_??/kubeconfig.yaml
      export KUBERNETES_SERVICE_HOST=k3s.crownstone-labs.com
      export KUBERNETES_SERVICE_PORT=6443

      export APISERVER_CERT_LOCATION=/see_previous_step/ca.crt
      export APISERVER_KEY_LOCATION=/see_previous_step/ca.key
      ```

For deployment as binary:

- a virtual-kubelet binary

  ```bash
  git clone https://gitlab.almende.org/research-projects/codeco/demonstrator.git
  cd demonstrator/virtual-kubelet
  go mod tidy
  make
  ```


- a Redis server

  One of

  - dockerized

    ```bash
    #start redis
    docker run -d --name redis -p 6379:6379 redis/redis-stack:latest

    #or run in foreground with:
    # docker run -p 6379:6379 -it redis/redis-stack:latest

    #verify status
    sudo apt install redis-tools
    redis-cli hello | head
    ```

  - podized
    ``` bash
    kubectl apply -f example/redis.yaml

    #verify status in Kind cluster
    sudo apt install redis-tools

    # lookup ip and port 
    kubectl describe pod redis | grep "Node:"
    kubectl get service redis

    # use found ip and port. EG: 
    redis-cli -h 172.18.0.2 -p 30379 ping
    ```

## Deployment as binary

1. write a config (json)

   ```bash
   cat stones_config.json
   ```

   ```json
   {
    "stonemac":
     { "cpu":"10",
       "memory":"10Gi",
       "pods":"1",
       "others": { "a":"4"  },
       "providerID":"5",
       "labels":{
         "kubernetes.io/arch":"Armv7",
         "kubernetes.io/os":"Bluenet"
       }
     }
   }
   ```

   note: this configfile can have multiple node-configs

1. start a specific vkubelet (on a specific port)

   ```bash
   ./bin/virtual-crownstone-kubelet \
     --nodename "stonemac" \
     --port 10256 \
     --provider sphere \
     --provider-config stones_config.json \
     --redis-addr "127.0.0.1:6379"
   ```

## Deployment as Pod

The virtual-kubelet image is availble on the almende docker registery, but can also be build locally

  ```bash
  eval $(minikube docker-env)
  docker build -t virtual-crownstone-kubelet:1 .
  ```

Then, create a `secret.yaml` file, containing the "ca.crt" and "ca.key", needed by the virtual-kubelet to register themselves on the cluster.

  ```bash
  kubectl create secret generic apiserver-certs \
    --from-file=vkubelet-crt.pem=$APISERVER_CERT_LOCATION \
    --from-file=vkubelet-key.pem=$APISERVER_KEY_LOCATION \
    --dry-run=client -o yaml > cfg/secret.yaml
  kubectl apply -f secret.yaml
  ```

  And add a generic service account.

  ```bash
  kubectl apply -f service-account.yaml
  ```

  Then, we deploy redis as a Pod, and listen to the kubernetes port it is exposed on.

  ```bash
  kubectl apply -f examples/redis.yaml
  minikube service redis --url &
  ```

  To deploy a new virtual-kubelet, we can apply a yaml describing the Crownstone on the kubernetes cluster.

  ```bash
  kubectl apply -f examples/aa-aa-aa-aa-aa-aa.yaml
  ```

  Verify that the nodes have been registered in the cluster.

  ```bash
  kubectl get nodes
  ```

## Uploading a Microapp Pod

  To upload a Microapp, simply apply a microapp pod to the cluster.

  ```bash
  kubectl apply -f example/microapp.yaml
  kubectl get pods -A
  ```

  To stop/delete a Microapp,

   ```bash
   kubectl delete pod microapp
   ```

   pedantic note: use podname instead of nodename!

---

## Appendix

tooling for debug purposes

- inspect a cluster with k9s 'textual user interface'

  ```bash
  cd ~/projects
  git clone https://github.com/derailed/k9s.git
  cd k9s/
  make build
  ./execs/k9s  #to exit use  :q
  ```

- kubectl testfile to target specific worker/node

   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
      name: dummyname

   spec:
     containers:
     - name: dummycontainername
       image: dummyimage
       env:
       - name: dummyvar
         value: "Hello Sander"

     #note: nodeName should match the targeted VK
     nodeName: stonemac
   ```
