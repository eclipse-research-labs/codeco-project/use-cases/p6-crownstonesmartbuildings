# Hub Scripts

## Overview

This directory contains the scripts responsible for:

1. Reading commands from the Redis message queue (MQ).
2. Executing these commands via BLE on Crownstone devices.
3. Collecting data from Crownstone devices and storing it in a local database.

## Usage

1. Start a redis MQ server (dockerized)

    ```bash
    #start redis
    docker run -p 6379:6379 -it redis/redis-stack:latest

    #verify status
    sudo apt install redis-tools
    redis-cli hello | head
    ```

2. Install python package `crownstone_hub` locally

    ```bash
    python install -r requirements.txt
    python install -e .
    ```

3. Run `crownstone_worker.py`

    ```bash
    python crownstone_hub/crownsone-hub.py --ble-settings /path/to/sphere_keys.json, --uart-device /dev/ttyACM0 --microapp-path /path/to/microapp.bin
    ```
