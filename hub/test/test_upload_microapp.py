 #
 # This file is part of the Crownstone distribution.
 # Copyright (c) Crownstone (https://crownstone.rocks)
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, version 3.
 #
 # This program is distributed in the hope that it will be useful, but
 # WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 # General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program. If not, see <http://www.gnu.org/licenses/>.
 #

import unittest
from unittest import IsolatedAsyncioTestCase

from redis import asyncio as aioredis
import json
import time

NODE_NAME = "CED40711-4DB1-E6B7-BF5C-0BE876F0EC84"
REDIS_REQ_QUEUE = "vk:requests"
REDIS_REPLY_QUEUE = f"vk:replies:{NODE_NAME}"

class TestUploadMicroapp(IsolatedAsyncioTestCase):

    async def test_send_redis_upload_microapp(self):
        print(self.test_send_redis_upload_microapp.__name__)
        redis_url = "redis://localhost"

        # Message to send
        message = {
            "id": "0382a0be-07ea-4337-8988-26c11cb47fb5",
            "at": time.time(),
            "node": NODE_NAME,
            "cmd": "uploadMicroapp",
            "args": {
                "image": "https://cloud.almende.org/s/NCFpjYLJtbaYibw/download/relay.bin"
            }
        }

        # Connect to Redis
        redis = await aioredis.from_url(redis_url)

        # Convert the message to a JSON string
        message_json = json.dumps(message)

        # Push the message onto the vk:request queue
        await redis.lpush(REDIS_REQ_QUEUE, message_json)
        print("Message sent to redis queue.")

        reply = await redis.blpop(REDIS_REPLY_QUEUE)
        self.assertEqual(len(reply), 2)
        reply = reply[1].decode("utf-8")
        reply = json.loads(reply)

        self.assertEqual(reply['id'], message["id"])
        self.assertGreater(reply['at'], message["at"])
        self.assertEqual(reply['result'], "Ok")

        await redis.close()

    async def test_send_redis_remove_microapp(self):
        print(self.test_send_redis_remove_microapp.__name__)
        redis_url = "redis://localhost"

        # Message to send
        message = {
            "id": "0382a0be-07ea-4337-8988-26c11cb47fb5",
            "at": time.time(),
            "node": NODE_NAME,
            "cmd": "removeMicroapp",
            "args": dict(),
        }

        # Connect to Redis
        redis = await aioredis.from_url(redis_url)

        # Convert the message to a JSON string
        message_json = json.dumps(message)

        # Push the message onto the vk:request queue
        await redis.lpush(REDIS_REQ_QUEUE, message_json)
        print("Message sent to redis queue.")

        reply = await redis.blpop(REDIS_REPLY_QUEUE)
        self.assertEqual(len(reply), 2)
        reply = reply[1].decode("utf-8")
        reply = json.loads(reply)

        self.assertEqual(reply['id'], message["id"])
        self.assertGreater(reply['at'], message["at"])
        self.assertEqual(reply['result'], "Ok")

        await redis.close()

if __name__ == "__main__":
    unittest.main()