#
# This file is part of the Crownstone distribution.
# Copyright (c) Crownstone (https://crownstone.rocks)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import logging

import urllib.request

from crownstone_hub.pipelines.ble import Ble

class MicroappManager:

    def __init__(self, ble: Ble):
        self.ble = ble

    async def tryUploadMicroapp(self, microapp_url, bleAddress: str) -> bool:
        # fetch the microapp binary
        try:
            with urllib.request.urlopen(microapp_url) as f:
                data = f.read()
        except Exception as e:
            logging.exception(f"failed to download microapp binary at {microapp_url}: {e}")
            return False

        logging.info(f"Successfully microapp binary: {len(data)} bytes")

        result = await self.ble.tryUploadMicroapp(data=data, bleAddress=bleAddress)

        if result:
            logging.info("Uploaded microapp successfull")
        else:
            logging.warning("Failed to upload microapp")

        return result

    async def tryRemoveMicroapp(self, bleAddress) -> bool:
        result = await self.ble.tryRemoveMicroapp(bleAddress=bleAddress)

        if result:
            logging.info("Removed microapp successfull")
        else:
            logging.warning("Failed to remove microapp")

        return result