#
# This file is part of the Crownstone distribution.
# Copyright (c) Crownstone (https://crownstone.rocks)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import asyncio
import logging
import sys
import time

from crownstone_hub.pipelines.ble import Ble
from crownstone_hub.pipelines.uart import Uart
from crownstone_hub.pipelines.redis import Redis, HubReplyMessage, HubRequestMessage

from crownstone_hub.cmd.microapp import MicroappManager

# Log configuration
LOG_FORMAT = "[%(asctime)s][%(filename)s][%(lineno)d] %(levelname)s: %(message)s"

def parse_args():
    """
    Parse command-line arguments using argparse.
    """
    parser = argparse.ArgumentParser(description="Crownstone Hub Script")
    parser.add_argument(
        "--log-file",
        type=str,
        default="logs/worker.log",
        help="Path to the log file (default: logs/worker.log)",
    )
    parser.add_argument(
        "--redis-host",
        type=str,
        default="localhost",
        help="Redis server hostname or IP address (default: localhost)",
    )
    parser.add_argument(
        "--redis-port",
        type=int,
        default=63799,
        help="Redis server port (default: 6379)",
    )
    parser.add_argument(
        "--ble-settings",
        type=str,
        required=True,
        help="Path to the BLE settings JSON file",
    )
    parser.add_argument(
        "--uart-device",
        type=str,
        required=False,
        help="Path to the UART device (e.g., /dev/ttyUSB0 or /dev/cu.usbmodemXXXXXX)",
    )
    return parser.parse_args()


def configure_logging(log_file):
    """
    Configure logging for the application.
    """
    logHandlers = []
    logHandlers.append(logging.FileHandler(log_file))
    logHandlers.append(logging.StreamHandler(sys.stdout))
    logging.basicConfig(level=logging.INFO, format=LOG_FORMAT, handlers=logHandlers)


async def main():
    """
    Main hub script: Reads commands from Redis queue and processes them one at a time.
    """
    args = parse_args()
    configure_logging(args.log_file)

    # Configure pipelines

    redis = Redis(f"redis://{args.redis_host}:{args.redis_port}")
    await redis.intialize()
    ble = Ble(settings_file=args.ble_settings)
    uart = Uart(args.uart_device)
    if args.uart_device:
        await uart.initialize() # disabled for now..
    else:
        logging.warning("UART device not initialized")

    # Configure cmd behaviour
    microapp = MicroappManager(ble)

    async def redis_callback(message: HubRequestMessage):
        bleAddress = message.node
        id = message.id
        cmd = message.cmd

        if cmd == "uploadMicroapp":
            url = message.args['image']
            result = await microapp.tryUploadMicroapp(url, bleAddress)
            reply = HubReplyMessage(id, time.time(), "Ok" if result else "Fail")
            await redis.send(f"vk:replies:{bleAddress}", reply)
            return

        if cmd == "removeMicroapp":
            result = await microapp.tryRemoveMicroapp(bleAddress)
            reply = HubReplyMessage(id, time.time(), "Ok" if result else "Fail")
            await redis.send(f"vk:replies:{bleAddress}", reply)
            return


    async def ble_callback(message):
        pass

    async def uart_callback(message):
        await asyncio.sleep(1)

    streams = []
    streams.append(redis.listen("vk:requests", redis_callback))
    streams.append(ble.listen(ble_callback))
    streams.append(uart.listen(uart_callback))

    await asyncio.gather(*streams)
    logging.info("Done! Exiting gracefully.")


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("Hub script terminated.")
