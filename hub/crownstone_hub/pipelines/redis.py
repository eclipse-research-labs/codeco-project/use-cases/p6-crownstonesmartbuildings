 #
 # This file is part of the Crownstone distribution.
 # Copyright (c) Crownstone (https://crownstone.rocks)
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, version 3.
 #
 # This program is distributed in the hope that it will be useful, but
 # WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 # General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program. If not, see <http://www.gnu.org/licenses/>.
 #

import logging
import asyncio
import json

from redis import asyncio as aioredis
from dataclasses import dataclass, asdict

@dataclass
class HubRequestMessage:
    """Message from the virtual-kubelet to hub daemon."""
    id: str
    at: int
    node: str
    cmd: str
    args: dict[str, str]

@dataclass
class HubReplyMessage:
    """Message reply to the virtual-kubelet."""
    id: str
    at: int
    result: str # OK or Fail are accepted.

class Redis:

    def __init__(self, server_url):
        self.server_url = server_url

    async def intialize(self):
        self.client = await aioredis.from_url(self.server_url)

    async def listen(self, queue, callback):
        while True:
            try:
                # Fetch the next command from the queue
                command = await self.client.lpop(queue)
                if command is None:
                    logging.debug("No commands in the queue. Waiting...")
                    await asyncio.sleep(1)  # Brief delay before retrying
                    continue

                # Decode the command (Redis stores as bytes)
                command = command.decode("utf-8")
                command = json.loads(command)
                request = HubRequestMessage(**command)
                logging.info(f"Received request: {request}")

                # Process the command
                await callback(request)

            except Exception as e:
                logging.exception(f"Error: {e}")
            finally:
                await asyncio.sleep(0.1)  # Prevent tight looping

    async def send(self, queue, msg: HubReplyMessage):
        command = json.dumps(asdict(msg))
        command.encode('utf-8')
        await self.client.lpush(queue, command)
