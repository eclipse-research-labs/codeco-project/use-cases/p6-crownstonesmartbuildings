 #
 # This file is part of the Crownstone distribution.
 # Copyright (c) Crownstone (https://crownstone.rocks)
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, version 3.
 #
 # This program is distributed in the hope that it will be useful, but
 # WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 # General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program. If not, see <http://www.gnu.org/licenses/>.
 #

import logging
import asyncio
from typing import Coroutine
from crownstone_ble import CrownstoneBle, BleEventBus, BleTopics

LOGGER = logging.getLogger(__name__)


class Ble:

    def __init__(self, settings_file: str):
        self.client = CrownstoneBle()
        self.client.loadSettingsFromFile(settings_file)

    async def listen(self, callback: Coroutine):
        def onData(data):
            asyncio.run_coroutine_threadsafe(callback(data), asyncio.get_running_loop())

        # In this case we've subscribed to the `BleTopics.newDataAvailable` topic. You could also subscribe to other events.
        BleEventBus.subscribe(BleTopics.newDataAvailable, onData)

        while True:
            await self.client.startScanning(60)
            await asyncio.sleep(1.2)

    async def tryUploadMicroapp(self, data, bleAddress) -> bool:
        index = 0
        protocol = 1

        try:
            if not await self.connectToCrownstone(bleAddress):
                LOGGER.warning("Connecting failed. Abort uploading microapp")
                return False
        except Exception as e:
            LOGGER.exception(e)
            LOGGER.warning("Could not connect to crownstone.")
            return False

        try:

            info = await self.client.microapp.getMicroappInfo()

            # If there is already some data at this index, it has to be removed first.
            if info.appsStatus[index].tests.hasData:
                print(f"Remove data at index {index}")
                await self.client.microapp.removeMicroapp(index, protocol)

            chunkSize = min(256, info.maxChunkSize)
            await self.client.microapp.uploadMicroapp(data, index, protocol, chunkSize)
            await self.client.microapp.validateMicroapp(index, protocol)
            await self.client.microapp.enableMicroapp(index, protocol)
            LOGGER.info(f"Uploaded microapp to {bleAddress}")
            return True
        except Exception as e:
            LOGGER.exception(e)
            return False
        finally:
            await self.disconnectFromCrownstone()

    async def tryRemoveMicroapp(self, bleAddress) -> bool:
        index = 0
        protocol = 1

        try:
            if not await self.connectToCrownstone(bleAddress):
                LOGGER.warning("Connecting failed. Abort uploading microapp")
                return False
        except Exception as e:
            LOGGER.exception(e)
            return False

        try:
            info = await self.client.microapp.getMicroappInfo()

            # If there is already some data at this index, it has to be removed first.
            if info.appsStatus[index].tests.hasData:
                print(f"Remove data at index {index}")
                await self.client.microapp.removeMicroapp(index, protocol)

            await self.client.microapp.removeMicroapp(index, protocol)
            LOGGER.info(f"Removed microapp to {bleAddress}")

            return True

        except Exception as e:
            LOGGER.exception(e)
            return False
        finally:
            await self.disconnectFromCrownstone()

    async def getMicroapp(self, bleAddress):
        if not await self.connectToCrownstone(bleAddress):
            LOGGER.warning("Connecting failed. Abort fetching microapp")
            return

        try:
            info = await self.client.microapp.getMicroappInfo()
            logging.info(info)
        finally:
            await self.disconnectFromCrownstone()

    async def connectToCrownstone(self, bleAddress):
        if self.client.ble.scanningActive:
            self.client.ble.abortScan()
            LOGGER.debug(f"Stopped scanning")
        await self.client.connect(bleAddress)
        if not await self.client.ble.is_connected():
            return False
        LOGGER.debug(f"Connected to {bleAddress}")
        return True

    async def disconnectFromCrownstone(self):
        await self.client.disconnect()
        LOGGER.debug("Disconnected from crownstone")
